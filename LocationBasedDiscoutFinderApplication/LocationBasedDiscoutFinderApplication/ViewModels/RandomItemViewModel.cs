﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LocationBasedDiscoutFinderApplication.Models;

namespace LocationBasedDiscoutFinderApplication.ViewModels
{
    public class RandomItemViewModel
    {
        public Item Item { get; set; }
        public List<Customer> Customers { get; set; }

    }
}