﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LocationBasedDiscoutFinderApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Enable Mvc Attribute route
            routes.MapMvcAttributeRoutes();
            
            ////Custom Route---Must be before default route----->Most Specific to most generic
            //routes.MapRoute(
            //    "ItemsByAddedDate",
            //    "items/added/{year}/{month}",
            //    new { controller = "Items", action = "ByAddeddate" },
            //    //For forcing user to add 2 digit for month and 4 digit for year
            //    new {year=@"\d{4}", month=@"\d{2}"}
            //    );

            //Default Router
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
