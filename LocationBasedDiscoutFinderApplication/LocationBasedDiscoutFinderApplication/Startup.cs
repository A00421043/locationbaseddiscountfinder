﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LocationBasedDiscoutFinderApplication.Startup))]
namespace LocationBasedDiscoutFinderApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
