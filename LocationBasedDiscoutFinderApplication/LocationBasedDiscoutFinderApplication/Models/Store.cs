﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocationBasedDiscoutFinderApplication.Models
{
    public class Store
    {
        public int Id { get; set; }
        public string Name { get; set; }
       public string Street { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Line { get; set; }

        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Province { get; set; }
           
    }
}