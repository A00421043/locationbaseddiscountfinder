namespace LocationBasedDiscoutFinderApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateStoreModelAddedCity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stores", "city", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Stores", "city");
        }
    }
}
