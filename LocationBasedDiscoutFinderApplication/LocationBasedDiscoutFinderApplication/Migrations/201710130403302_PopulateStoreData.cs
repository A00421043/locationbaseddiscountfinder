namespace LocationBasedDiscoutFinderApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateStoreData : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO STORES(NAME,STREET,LONGITUDE,LATITUDE,LINE,CITY,POSTALCODE,PROVINCE) VALUES('WALMART','6990 Mumford Rd',42.89,43.56,'','HALIFAX','B3L 4W4','NOVASCOTIA')");
            Sql("INSERT INTO STORES(NAME,STREET,LONGITUDE,LATITUDE,LINE,CITY,POSTALCODE,PROVINCE) VALUES('ATLANTIC SUPER STORE','6990 Mumford Rd',42.89,43.56,'','HALIFAX','B3L 4W4','NOVA SCOTIA')");
        }
        
        public override void Down()
        {
        }
    }
}
