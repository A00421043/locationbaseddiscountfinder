namespace LocationBasedDiscoutFinderApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateStoreModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Stores", "AddressId", "dbo.Addresses");
            DropIndex("dbo.Stores", new[] { "AddressId" });
            AddColumn("dbo.Stores", "Street", c => c.String());
            AddColumn("dbo.Stores", "Longitude", c => c.Double(nullable: false));
            AddColumn("dbo.Stores", "Latitude", c => c.Double(nullable: false));
            AddColumn("dbo.Stores", "Line", c => c.String());
            AddColumn("dbo.Stores", "PostalCode", c => c.String());
            AddColumn("dbo.Stores", "Province", c => c.String());
            DropColumn("dbo.Stores", "AddressId");
            DropTable("dbo.Addresses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Street = c.String(),
                        Longitude = c.Double(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Line = c.String(),
                        PostalCode = c.String(),
                        Province = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Stores", "AddressId", c => c.Int(nullable: false));
            DropColumn("dbo.Stores", "Province");
            DropColumn("dbo.Stores", "PostalCode");
            DropColumn("dbo.Stores", "Line");
            DropColumn("dbo.Stores", "Latitude");
            DropColumn("dbo.Stores", "Longitude");
            DropColumn("dbo.Stores", "Street");
            CreateIndex("dbo.Stores", "AddressId");
            AddForeignKey("dbo.Stores", "AddressId", "dbo.Addresses", "Id", cascadeDelete: true);
        }
    }
}
