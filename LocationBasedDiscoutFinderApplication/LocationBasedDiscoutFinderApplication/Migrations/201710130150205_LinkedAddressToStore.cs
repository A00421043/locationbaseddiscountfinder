namespace LocationBasedDiscoutFinderApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LinkedAddressToStore : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stores", "AddressId", c => c.Int(nullable: false));
            CreateIndex("dbo.Stores", "AddressId");
            AddForeignKey("dbo.Stores", "AddressId", "dbo.Addresses", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Stores", "AddressId", "dbo.Addresses");
            DropIndex("dbo.Stores", new[] { "AddressId" });
            DropColumn("dbo.Stores", "AddressId");
        }
    }
}
