﻿using LocationBasedDiscoutFinderApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using LocationBasedDiscoutFinderApplication.ViewModels;

namespace LocationBasedDiscoutFinderApplication.Controllers
{
    public class ItemsController : Controller
    {
        

        //Get: Item/Random
        public ActionResult Random()
        {
            //var item = new Item() { Name = "Milk" };
            //return View(item);
            var item = new Item() {Name = "Milk"};
            
            var customers = new List<Customer>
            {
                new Customer() {Name = "Nitin"},
                new Customer() {Name = "Janti"}
            };
            var viewModel = new RandomItemViewModel()
            {
                Item = item,
                Customers = customers
            };
            return View(viewModel);
        }

        public ActionResult Edit(int id)
        {
            return Content("Id= " + id);
        }
        // GET: Item
        public ActionResult Index( int? pageIndex, string sortBy)
        {
            var itemList = GetItems();
            return View(itemList);
            //if(!pageIndex.HasValue)
            //{
            //    pageIndex = 1;
            //}
            //if(String.IsNullOrWhiteSpace(sortBy))
            //{
            //    sortBy = "Name";
            //}
            //return Content(string.Format("Page Index={0} & Sort By={1}",pageIndex,sortBy));
        }

        [Route("items/added/{year}/{month:regex(\\d{2}):range(1,12)}")]
        public ActionResult ByAddeddate(int year,int month)
        {
            return Content("Year and month="+year+"/"+month);
        }

        public IEnumerable<Item> GetItems()
        {
            var itemList = new List<Item>
            {
                new Item {Id = 1, Name = "Milk"},
                new Item {Id = 2, Name = "Water"}
            };
            return itemList;
        }

        public ActionResult Detail(int id)
        {
            var item = GetItems().SingleOrDefault(i => i.Id == id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }
    }
}