﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LocationBasedDiscoutFinderApplication.Models;

namespace LocationBasedDiscoutFinderApplication.Controllers
{
    
    public class CustomersController : Controller
    {
        private ApplicationDbContext _Context;

        public CustomersController()
        {
            _Context=new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _Context.Dispose();
        }
        // GET: Customers
        public ActionResult Index()
        {

            var customerList = _Context.Customers.ToList();//GetCustomers();
            return View(customerList);
        }

        //public IEnumerable<Customer> GetCustomers()
        //{
        //    var customerList = new List<Customer>
        //    {
        //        new Customer() {Id=1,Name = "Nitin"},
        //        new Customer() {Id=2,Name = "Janti"}
        //    };
        //    return customerList;
        //}

        public ActionResult Detail(int id)
        {
            var customer = _Context.Customers.SingleOrDefault(c => c.Id == id);
            //var customer= GetCustomers().SingleOrDefault(c=>c.Id==id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }
    }
}