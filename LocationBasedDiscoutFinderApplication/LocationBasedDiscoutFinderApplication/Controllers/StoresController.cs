﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LocationBasedDiscoutFinderApplication.Models;

namespace LocationBasedDiscoutFinderApplication.Controllers
{
    public class StoresController : Controller
    {
        private ApplicationDbContext _Context;
        public StoresController()
        {
            _Context=new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _Context.Dispose();
        }
        // GET: Stores
        public ActionResult Index()
        {
            var listOfStore = _Context.Stores.ToList();
            //var listOfStore = GetStores();
            return View(listOfStore);
        }

        //public IEnumerable<Store> GetStores()
        //{
        //    var listOfStores = new List<Store>
        //    {
        //        new Store {Id = 1, Name = "Atlantic store"},
        //        new Store {Id = 2, Name = "Walmart"}
        //    };
        //    return listOfStores;
        //}

        public ActionResult Detail(int id)
        {
            //var store = GetStores().SingleOrDefault(s => s.Id == id);
            var store = _Context.Stores.SingleOrDefault(s => s.Id == id);
            if (store == null)
            {
                return HttpNotFound();
            }
            return View(store);
        }
    }
}